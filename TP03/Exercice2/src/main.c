#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <signal.h>

/* Global variables */
pid_t child;
pid_t father;
int end_game = 0;
int father_point = 0;
int child_point = 0;

/* Prototypes */

void sig_handler(int signal_no);
int ball_catched();

// Main method
int main(int argc, char **argv)
{

    srand(time(NULL));


    // create process
    pid_t process = fork();    
    if (process == -1)
    {
        perror("Fork failed !");
        exit(EXIT_FAILURE);
    }   
    else if (process == 0)
    {
        // child process
        child = getpid();

        signal(SIGUSR1, sig_handler);
        signal(SIGUSR2, sig_handler);
        signal(SIGTERM, sig_handler);
    }
    else
    {
        // father process
        signal(SIGUSR1, sig_handler);
        signal(SIGUSR2, sig_handler);
        signal(SIGTERM, sig_handler);

        wait(&process);

        father = getpid();
        kill(child, SIGUSR1);
        
    }

    exit(EXIT_SUCCESS);
}

/* Methods */

void sig_handler(int signal_no)
{
    printf("Signal reçus ! \n");
    int catched;

    switch (signal_no)
    {
        case SIGUSR1:
            // child process

            catched = ball_catched();
            printf("child : catched %d \n", catched);
            // when child catchs ball
            if (catched == 1)
            {
                sleep(1);
                kill(father, SIGUSR2);
            }
            // when child doesn't catch ball
            else
            {
                // father win;
                father_point++;
                sleep(1);
                if(father_point >= 13)
                {
                    printf("Father wins ! \n");
                    kill(child, SIGTERM);
                }
                else {               
                    printf("L'enfant sers !\n");
                    kill(father, SIGUSR2);   
                }
            }
            break;

        case SIGUSR2:
            
            catched = ball_catched();
            printf("father : catched %d \n", catched);
            // when father catchs ball
            if (catched == 1)
            {   
                sleep(1);
                kill(child, SIGUSR1);
            }
            // when father doesn't catch ball
            else
            {
                // child win;
                child_point++;
                sleep(1);
                if(child_point >= 13)
                {
                    printf("child wins ! \n");
                    kill(father, SIGTERM);
                }else {
                    printf("le parent sers !\n");
                    kill(child, SIGUSR1);
                }
            }            
            break;
        
        case SIGTERM:
            
            printf("Partie terminée ! \n");
            printf("Father point : %d ! \n", father_point);
            printf("Child point : %d ! \n", child_point);
            sleep(1);
            
            break;
    }
}

int ball_catched()
{
    return (rand() % 2);
}
