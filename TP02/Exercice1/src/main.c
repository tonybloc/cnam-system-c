#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <unistd.h>


int main(int argc, char** argv) {
    
    pid_t process = fork();
    if (process == 0) 
    {    
        pid_t pid = getpid();
        pid_t ppid = getppid();

        printf("Child : pid %d - ppid %d \n", pid, ppid);
        
        // exit last digit of pid
        exit(pid%10);
    }
    else 
    { 
        // print child pid - returned by wait method
        printf("Father : pid %d \n", wait(&process));
    }
    exit(EXIT_SUCCESS);
}