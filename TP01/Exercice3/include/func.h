#ifndef HEADER_FUNC
#define HEADER_FUNC

/**
 * Copy content of input file to output file
 * \parm src_input  - input file source
 * \parm src_output - output file source
 * \return : success (0) or failure (-1)
 */
int copy_file_to(char* src_input, char* src_output);

/**
 * Reverse the content of file and print it to prompt
 * \parm src - file source
 * \return : success (0) or failure (-1)
 */
int reverse_content_of_file(char* src) 

/**
 * Copy the behavior of ls command without options
 * \parm src - path (directory or file)
 * \return : success (0) or failure (-1)
 */
int ls_like_without_apt(char* src)


#endif