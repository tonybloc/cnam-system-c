#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <unistd.h>


int main(int argc, char** argv) {
    
    if (argc == 2) {
        printf("Command arguments %s \n", argv[1]);
    } else {
        printf("Programm take only one argument !");
    }

    exit(EXIT_SUCCESS);
}