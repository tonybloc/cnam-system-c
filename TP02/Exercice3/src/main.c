#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char** argv) {

    int process = fork();
    
    if (process == -1) 
    {
        perror("Fork Error !");
        exit(EXIT_FAILURE);
    } 
    else if (process == 0) {

        const char ps_command[] = "/bin/ps";
        const char grep_command[] = "/bin/grep";     

        // 0 : read
        // 1 : write 
        int fd[2]; 

        // define pipe between process (ps and grep command)
        if (pipe(fd) == -1)
        {
            perror("Pipe error !");
            exit(EXIT_FAILURE);
        }

        // create child process
        int cmd_process = fork();
        if (cmd_process == -1) 
        {
            perror("Fork Error !");
            exit(EXIT_FAILURE);
        }    
        else if (cmd_process == 0)
        {
            // close up input pipe and duplicate pipe input side to stdin
            dup2(fd[1], 1);
            close(fd[0]);
            close(fd[1]);
            
            // execute ps command with "eaux" opt
            int executed = 0;
            executed = execlp(ps_command, ps_command, "aux", (char*) NULL);                    
            if (executed == -1) {
                perror("Execution failed ! (ps command)");
            }
            
            exit(EXIT_SUCCESS);
        }
        else 
        {
            // Find grep process

            // update output source
            int fdevnull = open("/dev/null", O_WRONLY);
            dup2(fdevnull, 1); 

            // update input source
            dup2(fd[0],0);  
            
            // close up output pipe side      
            close(fd[0]);
            close(fd[1]);
            
            wait(&cmd_process);
            // execute grep command with "eaux" opt
            int executed = 0;
            executed = execlp(grep_command, grep_command, "^root", (char*) NULL);
            if (executed == -1) {
                perror("Execution failed ! (grep command)");
            }

            exit(EXIT_SUCCESS);
        }
    }
    else {

        // Wait child status
        wait(&process);

        // check status
        if (WIFEXITED(process) && ( WEXITSTATUS(process) == 0) ) {
            write(1, "Root et connecté \n",20);
        }

        exit(EXIT_SUCCESS);
    }
}